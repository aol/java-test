package com.yufei.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HellocController {
    @GetMapping("/hello")
    public String hello(){
        return "hello java-demo !!!!";
    }

}
